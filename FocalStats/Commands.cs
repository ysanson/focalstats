﻿using MetadataExtractor;
using MetadataExtractor.Formats.Exif;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FocalStats;
public class Commands : ConsoleAppBase
{
    readonly ILogger<Commands> logger;

    public Commands(ILogger<Commands> logger)
    {
        this.logger = logger;
    }

    private static int ExtractFocalLength(string filePath)
    {
        var subIfDirectory = ImageMetadataReader.ReadMetadata(filePath).OfType<ExifSubIfdDirectory>().FirstOrDefault();
        var focalLength = subIfDirectory?.GetDescription(ExifDirectoryBase.TagFocalLength);
        if (float.TryParse(focalLength?.Split(' ')[0], out float parsed))
        {
            return Convert.ToInt32(parsed);
        }
        else
        {
            return 0;
        }
    }

    [RootCommand]
    public void List(
        [Option("p", "The path to search in. Can be a folder or an image file.")] string path = ".", 
        [Option("t", "Searches for images in the top directory as well (recurse by default)")] bool top = false
    )
    {
        logger.LogDebug("Searching photos in folder {path}", path);
        if ((File.GetAttributes(path) & FileAttributes.Directory) == FileAttributes.Directory)
        {
            var photos = System.IO.Directory.EnumerateFiles(path, "*.jpg", top ? SearchOption.TopDirectoryOnly : SearchOption.AllDirectories)
                .AsParallel()
                .WithCancellation(Context.CancellationToken)
                .Select(file => ExtractFocalLength(file))
                .Where(focal => focal != 0)
                .Select(focal => Data.GetNearestFocalLength(focal))
                .GroupBy(f => f)
                .ToDictionary(obj => $"{obj.Key} mm" , obj => obj.Count())
                .OrderByDescending(x => x.Value);

            var graphBuilder = new AsciiBarGraph(photos);
            graphBuilder.WriteToConsole();
        }
        else
        {
            IEnumerable<MetadataExtractor.Directory> directories = ImageMetadataReader.ReadMetadata(path);
            var subIfDirectory = directories.OfType<ExifSubIfdDirectory>().FirstOrDefault();
            var focalLength = subIfDirectory?.GetDescription(ExifDirectoryBase.TagFocalLength);
            Console.WriteLine($"Focal length is {focalLength}");
        }
    }
}
