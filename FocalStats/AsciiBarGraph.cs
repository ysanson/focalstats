﻿using Cysharp.Text;
namespace FocalStats;

internal class AsciiBarGraph
{
    private readonly IEnumerable<KeyValuePair<string, int>> _items;

    public AsciiBarGraph(IEnumerable<KeyValuePair<string, int>> items)
    {
        _items = items;
    }

    private static int GetBarLength(int value, int maxValue, int maxLength)
    {
        return Convert.ToInt32((value / (double)maxValue) * maxLength);
    }

    public string BuildGraph(int maxBarLength)
    {
        if (!_items.Any()) return "";
        int maxLabelLength = _items.Max(x => x.Key.Length);
        int maxCount = _items.Max(x => x.Value);
        using var sb = ZString.CreateStringBuilder(true);
        foreach (var item in _items)
        {
            sb.AppendFormat("{0} | ", item.Key.PadLeft(maxLabelLength));
            for (int i = 0; i < GetBarLength(item.Value, maxCount, maxBarLength); i++)
            {
                sb.Append('█');
            }
            sb.AppendFormat(" {0}", item.Value);
            sb.AppendLine();
        }
        return sb.ToString();
    }

    public string BuildGraph()
    {
        return BuildGraph(Convert.ToInt32(Console.WindowWidth * 0.7));
    }

    public void WriteToConsole()
    {
        if (!_items.Any()) return;
        int maxLabelLength = _items.Max(x => x.Key.Length);
        int maxCount = _items.Max(x => x.Value);
        int maxBarLength = Convert.ToInt32(Console.WindowWidth * 0.7);
        foreach (var item in _items)
        {
            Console.Write("{0} | ", item.Key.PadLeft(maxLabelLength));
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            for (int i = 0; i < GetBarLength(item.Value, maxCount, maxBarLength); i++)
            {
                Console.Write('█');
            }
            Console.ResetColor();
            Console.WriteLine(" {0}", item.Value);
        }
    }
}
