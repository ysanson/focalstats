﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FocalStats;
public static class Data
{
    public static readonly int[] focals = { 10, 12, 14, 16, 18, 20, 24, 35, 50, 80 };

    public static int GetNearestFocalLength(int focal)
    {
        return focals.Select(f => (Math.Abs(f - focal), f))
            .OrderBy(f => f.Item1)
            .First()
            .f;
    }
}
