﻿using FocalStats;

var app = ConsoleApp.Create(args);
app.AddCommands<Commands>();
app.Run();
